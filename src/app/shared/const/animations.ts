import { trigger, animate, transition, style, query, } from '@angular/animations';

export const routeAnimation = trigger('routeAnimation', [

])

export const fadeInOut = trigger('fadeInOut', [
    transition(':enter', [
        style({ opacity: 0 }),
        animate(200, style({ opacity: 1 })),
    ]),
    transition(':leave', [animate(200, style({ opacity: 0 }))]),
]);

export const fadeIn = trigger('fadeIn', [
    transition(':enter', [
        style({ opacity: 0 }),
        animate(200, style({ opacity: 1 })),
    ]),
    transition(':leave', [animate(0, style({ opacity: 0 }))]),
]);