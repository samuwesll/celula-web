import { environment } from "src/environments/environment";

export class RelatorioURL {

    static BASE_URL = environment.base_url;

    static HTTP_CONSULTAR = RelatorioURL.BASE_URL + 'v1/relatorio/gerar';

}