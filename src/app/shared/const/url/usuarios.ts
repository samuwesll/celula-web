import { environment } from "src/environments/environment";

export class Usuario {

    static BASE_URL = environment.base_url;

    static HTTP_LISTA = Usuario.BASE_URL + 'v1/user';
    static HTTP_CADASTRAR = Usuario.BASE_URL + 'v1/user';
    static HTTP_LISTAR_POR_REDE = Usuario.BASE_URL + 'v1/usuarios/rede/';
}