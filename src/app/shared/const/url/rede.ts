import { environment } from "src/environments/environment";

export class Rede {

    static BASE_URL = environment.base_url;

    static HTTP_LISTAR = Rede.BASE_URL + 'v1/rede';
    static HTTP_CADASTRAR = Rede.BASE_URL + 'v1/rede';
    static HTTP_LISTAR_CORES = Rede.BASE_URL + 'v1/cor';

}