import { environment } from "src/environments/environment";

export class Perfil {

    static BASE_URL = environment.base_url;

    static HTTP_LISTA = Perfil.BASE_URL + 'v1/perfil';

}