import { environment } from "src/environments/environment";

export class CelulaURL {
    
    private static BASE_URL = environment.base_url;
    
    static HTTP_BASE = CelulaURL.BASE_URL + 'v1/celula';
    static HTTP_ASSOCIAR = CelulaURL.BASE_URL + 'v1/celula/associar'
}