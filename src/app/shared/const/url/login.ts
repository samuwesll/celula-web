import { environment } from "src/environments/environment";

export class Login {
    
    static BASE_URL = environment.base_url;
    
    static HTTP_GERAR_TOKEN = Login.BASE_URL + 'auth/login';
}