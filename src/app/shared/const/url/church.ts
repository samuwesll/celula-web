import { environment } from "src/environments/environment";

export class ChurchURL {
    
    static BASE_URL = environment.base_url;
    
    static HTTP_LISTAR = ChurchURL.BASE_URL + 'v1/church/lista' ;
    static HTTP_CADASTRAR = ChurchURL.BASE_URL + 'v1/church';
}