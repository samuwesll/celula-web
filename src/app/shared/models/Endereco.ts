export interface Endereco {
    cep: String,
    logradouro: String,
    numero: String,
    complemento: String, 
    bairro: String,
    estado: String,
    uf: String,
}