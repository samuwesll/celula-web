import { Response } from "./Response.model";

export interface Igreja {
    id?: Number;
    nome: String;
    estado: String;
    uf: String;
    cidade: String;
}

export interface IgrejaReponse extends Response<Igreja[]> {

}