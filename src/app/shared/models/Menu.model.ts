export interface Menu {
    nome: string,
    icone: string,
    rota: string,
    check: boolean,
}