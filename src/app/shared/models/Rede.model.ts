import { Igreja } from "./Igreja.model";
import { Response } from "./Response.model";

export interface Rede {
    id: Number;
    cor: String;
    igreja?: Igreja;
    pastores: String;
}

export interface Cor {
    id: number,
    cor: String,
    stiloCor: String,
}
export interface RedeCreate {
    cor: String,
    idIgreja: number,
    pastores: String,
    idCor: number,
}

export interface RedeResponse extends Response<Rede[]> {
    
}

export interface CorResponse extends Response<Cor[]> {

}