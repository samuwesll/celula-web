import { Endereco } from "./Endereco";
import { Membro } from "./Membro.model";
import { Response } from "./Response.model";
import { Usuario, UsuarioCelula } from "./Usuario.model";

export interface Celula {
    id: Number,
    nome: String,
    dia: String,
    horario: String,
    lideres: Usuario[]
    endereco: Endereco,
}

export interface CelulaRequest {
    nome: String,
    dia: String,
    horario: String,
    endereco: Endereco,
    idLideres: Array<number>,
    idSupers: Array<number>,
}

export interface CelulaDetalheResp {
    id: number,
    nome: String,
    dia: String,
    horario: String,
    endereco: Endereco
}

export interface CelulaDetalhe {
    celula: CelulaDetalheResp,
    membros: Array<Membro>,
    usuariosAssociados: Array<UsuarioCelula>
}

export interface CelulaResponse extends Response<Celula[]> {
    
}

export interface CelulaDetalheResponse extends Response<CelulaDetalhe> {

}