export interface Membro {
    id: Number,
    apelido: String,
    dataNacimento: String,
    nomeCompleto: String,
    nmCelular: String,
    perfil: String,
}