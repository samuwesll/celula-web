import { Response } from "./Response.model";

export interface RelatorioItem {
    nrSemana: number,
    data: String,
    membros: Number,
    quilos: Number,
    visitante: Number,
    observacao: String,
    ofertas: Number,
    status: boolean
}

export interface Relatorio {
    celula: string;
    lider: string;
    membros: number;
    visitante: number;
    ofertas: number;
    quilos: number;
    status: boolean,
    diasRelatorio?: Array<RelatorioItem>;
}

export interface RelatorioResponse extends Response<Array<Relatorio>> {}