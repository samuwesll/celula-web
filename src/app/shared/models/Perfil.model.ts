import { Response } from "./Response.model";

export interface Perfil {
    id: number;
    cargo: String;
}

export interface PerfilResponse extends Response<Array<Perfil>> {
    
}