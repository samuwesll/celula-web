import { Igreja } from "./Igreja.model";
import { Perfil } from "./Perfil.model";
import { Rede } from "./Rede.model";
import { Response } from "./Response.model";

interface Contato {
    email: String,
    celular: String
}

export interface Usuario {
    id?: Number;
    nomeCompleto: String;
    apelido: String;
    login: String;
    dataCadastro: Date;
    dataNascimento: Date;
    rede: Rede;
    igreja: Igreja;
    perfil: Perfil;
    contato: Contato;
}

export interface UsuarioCelula {
    id: Number,
    nomeCompleto: String,
    apelido: String,
    perfilUsuario: {
        id: Number,
        cargo: String,
    },
    papelNaCelula: {
        id: Number,
        cargo: String,
    }
}

export interface UsuarioRequest {
    apelido: String,
    contato: Contato,
    dataNascimento: Date,
    idPerfil: Number,
    idRede: Number,
    login: String,
    nomeCompleto: String,
    senha: String,
}

export interface UsuarioResumido {
    id: Number;
    nomeCompleto: String;
    apelido: String;
    perfil: Perfil;
}

export interface UsuarioResponse extends Response<Usuario[]> {}

export interface UsuarioRedeResponse extends Response<UsuarioResumido[]> {}