export interface Sucess<T> {
    mensage: String;
    value: T;
}

export interface Erros{
    error: String;
    mensagem: String;
}

export interface Response<T> {
    sucesso: Sucess<T>;
    errors: Erros[];
    timestamp: Date;
    status: Number;
}