import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../models/Usuario.model';
import { HeaderService } from '../../services/header.service';
import { StorageService } from '../../services/storage.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title: string;
  subtitle: string;

  usuario: Usuario;

  constructor(
    private headerService: HeaderService, 
    private router: Router,
    private localStorageService: StorageService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.inserirTitulo();
    this.carregarUsuario();
  }

  inserirTitulo(): void {
    this.headerService.title.subscribe(title => {
      this.title = title;
    })
    this.headerService.subtitle.subscribe(subtitle => {
      this.subtitle = subtitle;
    })
  }

  carregarUsuario(): void {
    this.localStorageService.getItem('USER').subscribe((user: Usuario) => {
      this.usuario = user;
    })
  }

  realizarLogout(content) {
    this.modalService.open(content, {windowClass: 'dark-modal'})
  }

  confirmarSaida() {
    this.localStorageService.limpar();
    this.router.navigateByUrl('login');
    this.modalService.dismissAll();
  }

}
