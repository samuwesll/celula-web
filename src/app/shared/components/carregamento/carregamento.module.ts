import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarregamentoComponent } from './carregamento.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [CarregamentoComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ],
  exports: [
    MatProgressSpinnerModule
  ]
})
export class CarregamentoModule { }
