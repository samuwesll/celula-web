import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { fadeInOut } from '../../const/animations';
import { CarregandoService } from '../../services/carregando.service';

@Component({
  selector: 'app-carregamento',
  templateUrl: './carregamento.component.html',
  styleUrls: ['./carregamento.component.scss'],
  animations: [fadeInOut]
})
export class CarregamentoComponent implements OnInit {

  mostrarCarregamento: BehaviorSubject<boolean>;

  constructor(private carregamentoService: CarregandoService) { }

  ngOnInit(): void {
    this.mostrarCarregamento = this.carregamentoService.mostrarCarregamento;
  }

}
