import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from '../../models/Menu.model';
import { fadeInOut } from '../../const/animations';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [fadeInOut]
})
export class SidebarComponent implements OnInit {

  clickedToggleMenu: boolean;
  rota: string;
  listaMenus: Array<Menu> = [
    {nome: 'relatorio', icone: 'space_dashboard', rota: 'relatorio', check: false},
    {nome: 'celulas', icone: 'share', rota: 'celulas', check: false},
    {nome: 'usuarios', icone: 'supervisor_account', rota: 'usuarios', check: false},
    {nome: 'redes', icone: 'device_hub', rota: 'redes', check: false},
    {nome: 'igrejas', icone: 'holiday_village', rota: 'igrejas', check: false},
  ]

  constructor(private route: Router, private storageService: StorageService) {}

  ngOnInit(): void {
    this.getUrl();
    this.consularMenuOculto();
  }

  toogleMenu() {
    this.clickedToggleMenu = !this.clickedToggleMenu;
    this.storageService.setItem('MENU_OCULTO', this.clickedToggleMenu);
  }

  consularMenuOculto(): void {
    this.storageService.getItem('MENU_OCULTO').subscribe(menu => {
      this.clickedToggleMenu = menu;
    })
  }

  getUrl() {
    var url: String = this.route.url.split("/")[1]
    this.listaMenus.forEach(menu => {
      if (menu.rota == url) {
        menu.check = true
      } else {
        menu.check = false
      }
    })
  }

  navegar(index: number): void {
    let url = this.listaMenus[index].rota;
    this.listaMenus.forEach((menu, i) => {
      if (i == index) {
        menu.check = true;
      } else {
        menu.check = false;
      }
    })
    this.route.navigateByUrl(url)
  }
}
