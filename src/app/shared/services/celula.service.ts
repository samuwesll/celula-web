import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CelulaURL } from '../const/url/celula';
import { CelulaDetalheResponse, CelulaRequest, CelulaResponse } from '../models/Celula';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class CelulaService {

  constructor(private http: HttpClient) { }

  listar(): Observable<CelulaResponse> {
    const url = CelulaURL.HTTP_BASE;
    return this.http.get<CelulaResponse>(url);
  }

  cadastrar(celulaRequest: CelulaRequest): Observable<Response<number>> {
    const url = CelulaURL.HTTP_BASE;
    return this.http.post<Response<number>>(url, celulaRequest);
  }

  associarLider(idCelula: number, idLider): Observable<Response<string>>{
    const url = `${CelulaURL.HTTP_ASSOCIAR}?idCelula=${idCelula}&idLider=${idLider}`;
    return this.http.patch<Response<string>>(url, {});
  }

  detalheCelulaId(idCelula: Number): Observable<CelulaDetalheResponse> {
    const url = `${CelulaURL.HTTP_BASE}/${idCelula}`;
    return this.http.get<CelulaDetalheResponse>(url);
  }

}
