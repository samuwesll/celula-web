import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChurchURL } from '../const/url/church';
import { Igreja, IgrejaReponse } from '../models/Igreja.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class ChurchService {

  constructor(private http: HttpClient) { }

  listarIgrejas(): Observable<IgrejaReponse> {
    let url = ChurchURL.HTTP_LISTAR;
    return this.http.get<IgrejaReponse>(url)
  }

  cadastrar(church: Igreja): Observable<Response<number>> {
    let url = ChurchURL.HTTP_CADASTRAR;
    return this.http.post<Response<number>>(url, church)
  }
}
