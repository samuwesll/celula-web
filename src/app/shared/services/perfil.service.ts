import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Perfil } from '../const/url/Perfil';
import { PerfilResponse } from '../models/Perfil.model';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  constructor(private http: HttpClient) { }

  listar(): Observable<PerfilResponse> {
    const url = Perfil.HTTP_LISTA;
    return this.http.get<PerfilResponse>(url);
  }

}
