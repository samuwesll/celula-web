import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public title = new BehaviorSubject<string>('Title');
  public subtitle = new BehaviorSubject<string>('');

  constructor() { }

  setTitle(title: string) {
    this.title.next(title);
  }

  setSubTitle(subtitle: string) {
    this.subtitle.next(subtitle);
  }
}
