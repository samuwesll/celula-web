import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setItem(chave: string, valor: any): void {
    localStorage.setItem(chave, JSON.stringify(valor));
  }

  removeItems(...chaves: string[]): void {
    chaves.forEach((chave) => this.removeItem(chave));
  }

  contem(chave: string): boolean {
    return !!localStorage.getItem(chave);
  }

  getItem(chave: string): Observable<any> {
    if (this.contem(chave)) {
      const item = localStorage.getItem(chave);
      const itemJson = this.converterEmJson(item);
      return of(itemJson);
    }

    return EMPTY;
  }

  removeItem(chave: string): void {
    localStorage.removeItem(chave);
  }

  private converterEmJson(dado: string): any {
    try {
      return JSON.parse(dado);
    } catch (e) {
      return dado;
    }
  }

  limpar(): void {
    localStorage.clear();
  }
}
