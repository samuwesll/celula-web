import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rede } from '../const/url/rede';
import { RedeCreate, RedeResponse, Rede as RedeP, CorResponse } from '../models/Rede.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class RedeService {

  constructor(private http: HttpClient) {}

  listarRede(): Observable<RedeResponse> {
    const url = Rede.HTTP_LISTAR;
    return this.http.get<RedeResponse>(url);
  }

  cadastrar(rede: RedeCreate): Observable<Response<RedeP>> {
    const url = Rede.HTTP_CADASTRAR;
    return this.http.post<Response<RedeP>>(url, rede); 
  }

  listarCores(): Observable<CorResponse> {
    const url = Rede.HTTP_LISTAR_CORES;
    return this.http.get<CorResponse>(url);
  }

}
