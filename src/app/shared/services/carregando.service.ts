import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarregandoService {

  readonly mostrarCarregamento = new BehaviorSubject<boolean>(false);

  constructor() { }

  mostrar(): void {
    this.mostrarCarregamento.next(true);
  }

  ocultar(): void {
    this.mostrarCarregamento.next(false);
  }
}
