import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../const/url/usuarios';
import { Response } from '../models/Response.model';
import { UsuarioRedeResponse, UsuarioRequest, UsuarioResponse } from '../models/Usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) { }

  listarTodos(): Observable<UsuarioResponse> {
    const url = Usuario.HTTP_LISTA;
    return this.http.get<UsuarioResponse>(url);
  }

  cadastrar(usuario: UsuarioRequest) :Observable<Response<Number>> {
    const url = Usuario.HTTP_CADASTRAR;
    return this.http.post<Response<Number>>(url, usuario);
  }

  listarPorRede(idRede: Number): Observable<UsuarioRedeResponse> {
    const url = Usuario.HTTP_LISTAR_POR_REDE + idRede;
    return this.http.get<UsuarioRedeResponse>(url);
  }
}
