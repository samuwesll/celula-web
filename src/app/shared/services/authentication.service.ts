import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from '../const/url/login';
import { TokenResponse, Credenciais } from '../models/Authentication.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  realizarLogin(credencys: Credenciais ): Observable<TokenResponse> {
    let url = Login.HTTP_GERAR_TOKEN;
    return this.http.post<TokenResponse>(url, credencys)
  }
}
