import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RelatorioURL } from '../const/url/relatorio';
import { RelatorioResponse } from '../models/Relatorio.model';

@Injectable({
  providedIn: 'root'
})
export class RelatorioService {

  constructor(private http: HttpClient) { }

  gerarRelatorio(search: String): Observable<RelatorioResponse> {
    const url = RelatorioURL.HTTP_CONSULTAR + search;
    return this.http.get<RelatorioResponse>(url);
  }
}
