import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastrarCelulaComponent } from './main/celula/cadastrar-celula/cadastrar-celula.component';
import { CelulaComponent } from './main/celula/celula.component';
import { ConsultaCelulaComponent } from './main/celula/consulta-celula/consulta-celula.component';
import { DetalheCelulaComponent } from './main/celula/detalhe-celula/detalhe-celula.component';
import { ChurchComponent } from './main/church/church.component';
import { ConsultaComponent } from './main/church/consulta/consulta.component';
import { InicioComponent } from './main/inicio/inicio.component';
import { LoginComponent } from './main/login/login.component';
import { MainComponent } from './main/main.component';
import { ConsultaRedeComponent } from './main/rede/consulta-rede/consulta-rede.component';
import { RedeComponent } from './main/rede/rede.component';
import { ConsultaRelatorioComponent } from './main/relatorio/consulta-relatorio/consulta-relatorio.component';
import { RelatorioComponent } from './main/relatorio/relatorio.component';
import { CadastroUsuarioComponent } from './main/usuario/cadastro-usuario/cadastro-usuario.component';
import { ConsultaUsuarioComponent } from './main/usuario/consulta-usuario/consulta-usuario.component';
import { UsuarioComponent } from './main/usuario/usuario.component';
import { AuthGuardService } from './shared/services/auth-guard.service';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', pathMatch: 'full', component: LoginComponent},
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuardService],
    children: [
      {path: 'inicio', component: InicioComponent},
      {
        path: 'igrejas',
        component: ChurchComponent,
        children: [
          {
            path: '',
            redirectTo: '/igrejas/consulta',
            pathMatch: 'full',
          },
          { path: 'consulta', component: ConsultaComponent }
        ]
      },
      {
        path: 'redes',
        component: RedeComponent,
        children: [
          {
            path: '',
            redirectTo: '/redes/consulta',
            pathMatch: 'full',
          },
          { path: 'consulta', component: ConsultaRedeComponent }
        ]
      },
      {
        path: 'usuarios',
        component: UsuarioComponent,
        children: [
          {
            path: '',
            redirectTo: '/usuarios/consulta',
            pathMatch: 'full'
          },
          { path: 'consulta', component: ConsultaUsuarioComponent},
          { path: 'cadastrar', component: CadastroUsuarioComponent }
        ]
      },
      {
        path: 'celulas',
        component: CelulaComponent,
        children: [
          {
            path: '',
            redirectTo: '/celulas/consulta',
            pathMatch: 'full'
          },
          { path: 'consulta', component: ConsultaCelulaComponent },
          { path: 'cadastrar', component: CadastrarCelulaComponent },
          { path: 'detalhe/:id', component: DetalheCelulaComponent }
        ]
      },
      {
        path: 'relatorio',
        component: RelatorioComponent,
        children: [
          {
            path: '',
            redirectTo: '/relatorio/consulta',
            pathMatch: 'full'
          },
          { path: 'consulta', component: ConsultaRelatorioComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
