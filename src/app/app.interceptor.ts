import { Injectable, NgModule } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { StorageService } from './shared/services/storage.service';
import { Login } from './shared/const/url/login';

@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(private route: Router, private storageService: StorageService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token: string;
    this.storageService.getItem('TOKEN').subscribe((t: string) => {
      if (t != null) {
        token = 'Bearer ' + t.toString();
      } 
    })
    if (request.url != Login.HTTP_GERAR_TOKEN) {
      request = request.clone({
        headers: request.headers.set('authorization', token)
      });
    }
    return next.handle(request);
  }
}

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true,
    },
  ],
})
export class Interceptor {}
