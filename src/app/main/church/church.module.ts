import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaComponent } from './consulta/consulta.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChurchComponent } from './church.component';

import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [ChurchComponent, ConsultaComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatInputModule,
    MatDialogModule
  ],
  exports: [
    MatInputModule,
    MatDialogModule
  ]
})
export class ChurchModule { }
