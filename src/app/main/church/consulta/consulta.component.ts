import { Component, OnInit } from '@angular/core';
import { ChurchService } from 'src/app/shared/services/church.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Igreja } from 'src/app/shared/models/Igreja.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  listaIgrejas: Array<Igreja> = [];
  formCadastrarIgreja: FormGroup;

  constructor(
    private churchService: ChurchService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.carregarIgrejas();
    this.criarFormulario();
  }

  criarFormulario(): void {
    this.formCadastrarIgreja = this.formBuilder.group({
      nome: ['', Validators.required, Validators.minLength(10),],
      cidade: ['',  Validators.required],
      estado: ['',  Validators.required],
      uf: ['',  Validators.required]
    })
  }

  carregarIgrejas(): void {
    this.churchService.listarIgrejas().subscribe(response => {
      this.listaIgrejas = response.sucesso.value;
    }, error => {
      this.openSnackBar(error.error.mensagem, 'Erro')
    })
  }

  cadastrarIgreja(): void {
    const { nome, cidade, estado, uf } =  this.formCadastrarIgreja.value;
    const id = this.listaIgrejas.length + 1;
    const igreja: Igreja = { nome, cidade, estado, uf, id };
    this.churchService.cadastrar(igreja).subscribe(response => {
      igreja.id = response.sucesso.value;
      this.listaIgrejas.push(igreja);
      this.openSnackBar(response.sucesso.mensage.toString(), 'Cadastro realizado')
    }, error => {
      this.openSnackBar(error, 'Cadastro realizado')
    }).add(
      this.dialog.closeAll()
    )
  }

  openSnackBar(mensagem: string, title: string): void {
    this._snackBar.open(mensagem, title, {
      data: 'csdmksdfksdm',
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    })
  }

  openDialog(content) {
    this.dialog.open(content);
  }

}
