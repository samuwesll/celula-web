import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { MainComponent } from './main.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { InicioComponent } from './inicio/inicio.component';
import { ChurchModule } from './church/church.module';
import { RedeModule } from './rede/rede.module';
import { UsuarioModule } from './usuario/usuario.module';
import { CelulaModule } from './celula/celula.module';
import { RelatorioModule } from './relatorio/relatorio.module';

@NgModule({
  declarations: [MainComponent, LoginComponent, InicioComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatBottomSheetModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule,
    MatSnackBarModule,
    SharedModule,
    ChurchModule,
    RedeModule,
    UsuarioModule,
    CelulaModule,
    RelatorioModule
  ],
  exports: [
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatBottomSheetModule,
    FormsModule, 
    ReactiveFormsModule,
    MatSnackBarModule,
    SharedModule,
    RedeModule,
    UsuarioModule,
    CelulaModule,
    RelatorioModule
  ]
})
export class MainModule { }
