import { Component, EventEmitter, OnInit } from '@angular/core';
import { fadeInOut } from 'src/app/shared/const/animations';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RedeService } from 'src/app/shared/services/rede.service';
import { ChurchService } from 'src/app/shared/services/church.service';
import { Rede } from 'src/app/shared/models/Rede.model';
import { Igreja } from 'src/app/shared/models/Igreja.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Perfil } from 'src/app/shared/models/Perfil.model';
import { PerfilService } from 'src/app/shared/services/perfil.service';
import { Usuario, UsuarioRequest } from 'src/app/shared/models/Usuario.model'
import { UsuariosService } from 'src/app/shared/services/usuarios.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.scss'],
  animations: [fadeInOut],
})
export class CadastroUsuarioComponent implements OnInit {

  formUsuario: FormGroup;
  listaRede: Rede[];
  listaRedeFiltrada: Rede[];
  listaIgrejas: Array<Igreja>;
  listaPerfis: Array<Perfil>;
  listaPerfisFiltrados: Array<Perfil>;
  usuario: Usuario;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private formBuilder: FormBuilder,
    private redeService: RedeService,
    private igrejaService: ChurchService,
    private perfilService: PerfilService,
    private _snackBar: MatSnackBar,
    private usuarioService: UsuariosService,
    private localStorageService: StorageService,
  ) { }

  ngOnInit(): void {
    this.criarFormulario();
    this.carregarUsuario(); 
  }

  criarFormulario(): void {
    this.formUsuario = this.formBuilder.group({
      nomeCompleto: ['', Validators.required],
      login: ['', Validators.required],
      senha: ['', Validators.required],
      apelido: ['', Validators.required],
      dataNascimento: ['', Validators.required],
      idRede: [{ value: '', disabled: true }, Validators.required],
      idPerfil: ['', Validators.required],
      celular: ['', Validators.required],
      email: ['', Validators.required],
      idIgreja: ['']
    })
  }

  carregarRedes(): void {
    this.redeService.listarRede().subscribe(response => {
      this.listaRede = response.sucesso.value;
      this.carregarIgrejas();
    })
  }

  carregarIgrejas(): void {
    this.igrejaService.listarIgrejas().subscribe(response => {
      this.listaIgrejas = response.sucesso.value;
      this.carregarPerfis();
    })
  }

  carregarPerfis(): void {
    this.perfilService.listar().subscribe(response => {
      this.listaPerfis = response.sucesso.value;
      this.filtroPorUser();
    })
  }

  openDialog(content): void {
    this.dialog.open(content);
  }

  btnVoltar(): void {
    this.router.navigateByUrl('usuarios/consulta')
  }

  selecionarIgreja() {
    const { idIgreja } = this.formUsuario.value
    this.listaRedeFiltrada = this.listaRede.filter(rede => rede.igreja.id == idIgreja)
    if (this.listaRedeFiltrada.length == 0) {
      this.openSnackBar("Não há redes cadastrada para essa igreja", "Erro");
      this.formUsuario.get('idRede').setValue('');
      this.formUsuario.get('idRede').disable({ emitEvent: false })
    } else {
      this.formUsuario.get('idRede').setValue('');
      this.formUsuario.get('idRede').enable({ emitEvent: true })
    }
  }

  openSnackBar(mensagem: string, title: string) {
    this._snackBar.open(mensagem, title, {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    })
  }

  cadastrarUsuario() {
    const {
      nomeCompleto,
      login,
      senha,
      apelido,
      dataNascimento,
      idRede,
      idPerfil,
      celular,
      email,
    } = this.formUsuario.value;
   var usuario: UsuarioRequest = {
     apelido,
     contato: { celular, email },
     dataNascimento,
     idPerfil,
     idRede,
     login,
     nomeCompleto,
     senha
   }
   this.usuarioService.cadastrar(usuario).subscribe(response => {
     this.openSnackBar(response.sucesso.mensage + "", "Cadastro ok")
     this.router.navigateByUrl('usuarios/consulta')
   }, erro => {
    
   })
  }

  carregarUsuario(): void {
    this.localStorageService.getItem('USER').subscribe((user: Usuario) => {
      this.usuario = user;
      this.carregarRedes();
    })
  }

  filtroPorUser(): void {
    if (this.usuario.perfil.id == 1) {
      this.listaPerfisFiltrados = this.listaPerfis;
    } else {
      this.listaPerfisFiltrados = this.listaPerfis.filter(l => l.id >= this.usuario.perfil.id)
    }

    if ("Diacono" == this.usuario.perfil.cargo) {
      this.formUsuario.get('idIgreja').setValue(this.usuario.igreja.id);
      this.formUsuario.get('idIgreja').disable({onlySelf: true});
      this.selecionarIgreja();
      this.formUsuario.get('idRede').setValue(this.usuario.rede.id);
      this.formUsuario.get('idRede').disable({onlySelf: true});
    }
  }

}
