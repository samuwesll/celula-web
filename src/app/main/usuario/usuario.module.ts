import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioComponent } from './usuario.component';
import { ConsultaUsuarioComponent } from './consulta-usuario/consulta-usuario.component';
import { RouterModule } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SharedModule } from 'src/app/shared/shared.module';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';

@NgModule({
  declarations: [UsuarioComponent, ConsultaUsuarioComponent, CadastroUsuarioComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatPaginatorModule,
    SharedModule,
  ],
  exports: [SharedModule]
})
export class UsuarioModule { }
