import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Igreja } from 'src/app/shared/models/Igreja.model';
import { Perfil } from 'src/app/shared/models/Perfil.model';
import { Usuario } from 'src/app/shared/models/Usuario.model';
import { ChurchService } from 'src/app/shared/services/church.service';
import { PerfilService } from 'src/app/shared/services/perfil.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { UsuariosService } from 'src/app/shared/services/usuarios.service';

@Component({
  selector: 'app-consulta-usuario',
  templateUrl: './consulta-usuario.component.html',
  styleUrls: ['./consulta-usuario.component.scss']
})
export class ConsultaUsuarioComponent implements OnInit {

  listaUsuarios: Array<Usuario>;
  listaUsuariosFilter: Array<Usuario>;
  formGroup: FormGroup;
  liderSelected: Usuario;
  idLiderSelecionado: Number;
  listaPerfis: Array<Perfil>;
  listaIgrejas: Array<Igreja>;
  usuario: Usuario;

  length = 0;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25, 50];
  showFirstLastButtons = true;

  inicio = 0;
  fim = 10;

  constructor(
    private usuarioService: UsuariosService,
    private router: Router,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private perfilService: PerfilService,
    private igrejaService: ChurchService,
    private localStorageService: StorageService,
  ) { }

  ngOnInit(): void {
    this.criarFormulario();
    this.carregarUsuarios();
    this.carregarUsuario();
  }

  criarFormulario(): void {
    this.formGroup = this.formBuilder.group({
      nomeCompleto: [''],
      idPerfil: [''],
      idIgreja: ['']
    })
  }

  carregarPerfis(): void {
    this.perfilService.listar().subscribe(response => {
      this.listaPerfis = response.sucesso.value;
      this.carregarIgrejas();
    })
  }

  carregarIgrejas(): void {
    this.igrejaService.listarIgrejas().subscribe(response => {
      this.listaIgrejas = response.sucesso.value;
      this.filtroPorUser();
    })

  }

  carregarUsuarios(): void {
    this.usuarioService.listarTodos().subscribe(response => {
      this.listaUsuarios = response.sucesso.value.filter(r => r.perfil.cargo != 'Admin');
      this.listaUsuariosFilter = this.listaUsuarios;
      this.length = this.listaUsuariosFilter.length
    })
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.fim = event.pageSize;
    this.inicio = this.pageIndex * this.pageSize;
    this.fim = this.inicio + this.pageSize;
  }

  telaCadastro(): void {
    this.router.navigateByUrl('usuarios/cadastrar')
  }

  openDialog(content, lider: Usuario) {
    this.dialog.open(content);
    this.liderSelected = lider;
    this.formGroup.controls.idLider.setValue(lider.nomeCompleto);
    this.idLiderSelecionado = lider.id;
  }

  openSnackBar(mensagem: string, title: string) {
    this._snackBar.open(mensagem, title, {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    })
  }

  filtrarUsuarios(): void {
    const { nomeCompleto, idPerfil, idIgreja } = this.formGroup.value;

    if (idPerfil) {
      this.listaUsuariosFilter = this.listaUsuarios.filter(user => user.perfil.id == idPerfil)
    }

    if (idIgreja) {
      this.listaUsuariosFilter = this.listaUsuarios.filter(user => user.igreja.id == idIgreja)
    }

    if(!nomeCompleto && !idPerfil && !idIgreja) {
      this.listaUsuariosFilter = this.listaUsuarios;
    }

    if (nomeCompleto) {
      this.listaUsuariosFilter = this.listaUsuarios
        .filter(user => user.nomeCompleto.toLowerCase().indexOf(nomeCompleto.toLowerCase()) > -1)
    }

    this.length = this.listaUsuariosFilter.length
  }

  carregarUsuario(): void {
    this.localStorageService.getItem('USER').subscribe((user: Usuario) => {
      this.usuario = user;
      this.carregarPerfis();
    })
  }

  filtroPorUser(): void {
    if ("Diacono" == this.usuario.perfil.cargo) {
      this.formGroup.get('idIgreja').setValue(this.usuario.igreja.id);
      this.formGroup.get('idIgreja').disable({onlySelf: true});
      this.formGroup.get('idPerfil').disable({onlySelf: true});
      this.filtrarUsuarios();
    }
  }
}
