import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedeComponent } from './rede.component';
import { ConsultaRedeComponent } from './consulta-rede/consulta-rede.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [RedeComponent, ConsultaRedeComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule
  ],
  exports: [
    MatInputModule,
    MatDialogModule,
    MatSelectModule
  ]
})
export class RedeModule { }
