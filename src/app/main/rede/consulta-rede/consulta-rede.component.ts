import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Igreja } from 'src/app/shared/models/Igreja.model';
import { Cor, Rede, RedeCreate } from 'src/app/shared/models/Rede.model';
import { ChurchService } from 'src/app/shared/services/church.service';
import { RedeService } from 'src/app/shared/services/rede.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-consulta-rede',
  templateUrl: './consulta-rede.component.html',
  styleUrls: ['./consulta-rede.component.scss']
})
export class ConsultaRedeComponent implements OnInit {

  listaRedes: Array<Rede>;
  igrejas: Array<Igreja>;
  formGroup: FormGroup;
  cores: Array<Cor>

  length = 0;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;

  inicio = 0;
  fim = 10;

  constructor(
    private redeService: RedeService,
    private churcService: ChurchService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.carregarRedes();
    this.carregarIgrejas();
    this.criarFormulario();
    this.carregarCoresRedes();
  }

  criarFormulario(): void {
    this.formGroup = this.formBuilder.group({
      idCor: ['', Validators.required],
      idIgreja: ['', Validators.required],
      pastores: ['']
    })
  }

  carregarCoresRedes(): void {
    this.redeService.listarCores().subscribe(r => {
      this.cores = r.sucesso.value;
    })
  }

  carregarRedes(): void {
    this.redeService.listarRede().subscribe(response => {
      this.listaRedes = response.sucesso.value;
      this.length = response.sucesso.value.length
    })
  }

  carregarIgrejas(): void {
    this.churcService.listarIgrejas().subscribe(response => {
      this.igrejas = response.sucesso.value;
    })
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.fim = event.pageSize;
    this.inicio = this.pageIndex * this.pageSize;
    this.fim = this.inicio + this.pageSize;
  }

  openDialog(content) {
    this.dialog.open(content);
  }

  cadastrarRede(): void {
    const { idCor, idIgreja, pastores } = this.formGroup.value
    var cor: String = this.cores.filter(v => v.id == idCor)[0].cor;
    const rede: RedeCreate = { idCor, idIgreja, pastores, cor };
    this.redeService.cadastrar(rede).subscribe(response => {
      this.listaRedes.push(response.sucesso.value)
    }).add(
      this.dialog.closeAll()
    )
  }

}
