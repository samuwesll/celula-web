import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  credencial: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private storage: StorageService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.verificarLogin();
    this.criarFormulario();
  }

  criarFormulario() {
    this.credencial = this.formBuilder.group({
      login: ['', Validators.required],
      senha: ['', Validators.required],
    })
  }

  verificarCredenciais() {
    if (this.credencial.invalid) {
      return
    }
    const { login, senha } = this.credencial.value;
    this.authenticationService.realizarLogin({ login, senha }).subscribe(resp => {
      this.storage.setItem("TOKEN", resp.sucesso.value.token);
      this.storage.setItem("USER", resp.sucesso.value.user);
      this.router.navigateByUrl('inicio')
    }, error => {
      this.openSnackBar(error.error.errors[0].mensagem, 'Erro')
    })
  }

  openSnackBar(mensagem: string, title: string) {
    this._snackBar.open(mensagem, title, {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    })
  }

  verificarLogin() {
    this.storage.getItem('TOKEN').subscribe(resp => {
      if (resp != null) {
        this.router.navigateByUrl('inicio')
      }
    })
  }

}
