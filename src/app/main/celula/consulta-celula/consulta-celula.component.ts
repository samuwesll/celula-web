import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Celula } from 'src/app/shared/models/Celula';
import { Usuario } from 'src/app/shared/models/Usuario.model';
import { CelulaService } from 'src/app/shared/services/celula.service';

@Component({
  selector: 'app-consulta-celula',
  templateUrl: './consulta-celula.component.html',
  styleUrls: ['./consulta-celula.component.scss']
})
export class ConsultaCelulaComponent implements OnInit {

  listaCelulas: Array<Celula>;

  length = 0;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;

  inicio = 0;
  fim = 10;

  constructor(
    private celulaService: CelulaService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.carregarCelulas();
  }

  carregarCelulas(): void {
    this.celulaService.listar().subscribe(response => {
      this.listaCelulas = response.sucesso.value;
      this.length = this.listaCelulas.length;
    })
  }

  apresentarLideres(lideres: Array<Usuario>): String {
    var nomes: String;
    if (lideres.length == 0) {
      return ''
    }
    lideres.forEach((lider, id) => {
      let primeiroNome: String = lider.nomeCompleto.split(" ")[0];
      id == 0 
        ? nomes = primeiroNome 
        : (id +1) == lideres.length ? nomes = nomes + ` e ${primeiroNome}`
        : nomes = nomes + `, ${primeiroNome}`
    });
    return nomes;
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.fim = event.pageSize;
    this.inicio = this.pageIndex * this.pageSize;
    this.fim = this.inicio + this.pageSize;
  }

  telaCadastrado(): void {
    this.router.navigateByUrl('celulas/cadastrar')
  }

  detalheCelula(idCelula: number): void {
    this.router.navigateByUrl(`celulas/detalhe/${idCelula}`)
  }

}
