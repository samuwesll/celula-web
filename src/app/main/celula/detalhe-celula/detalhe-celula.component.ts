import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CelulaDetalhe } from 'src/app/shared/models/Celula';
import { CelulaService } from 'src/app/shared/services/celula.service';

@Component({
  selector: 'app-detalhe-celula',
  templateUrl: './detalhe-celula.component.html',
  styleUrls: ['./detalhe-celula.component.scss']
})
export class DetalheCelulaComponent implements OnInit {

  step = 0;
  urlIdTelevenda: number;
  celulaDetalhe: CelulaDetalhe;
  formCelula: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private celulaService: CelulaService,
    private formBuilder: FormBuilder,
    ) {
      this.inserirCelula()
    }

  setStep(index: number) {
    this.step = index;
  }

  criarFormulario(): void {
    this.formCelula = this.formBuilder.group({
      nome: [''],
      horario: [''],
      dia: [''],
      cep: [''],
      logradouro: [''],
      numero: [''],
      complemento: [''],
      bairro: [''],
      estado: [''],
      uf: ['']
    })
    this.formCelula.disable();
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  ngOnInit(): void {
    this.criarFormulario();
  }

  btnVoltar(): void {
    this.router.navigateByUrl('celulas')
  }

  inserirCelula(): void {
    this.route.params.subscribe(res => {
      this.urlIdTelevenda = Number(res.id);
      this.consultaCelulaId(this.urlIdTelevenda)
    })
  }

  consultaCelulaId(idCelula: Number): void {
    this.celulaService.detalheCelulaId(idCelula).subscribe(resp => {
      this.celulaDetalhe = resp.sucesso.value;

      this.formCelula.get('nome').setValue(this.celulaDetalhe.celula.nome);
      this.formCelula.get('horario').setValue(this.celulaDetalhe.celula.horario);
      this.formCelula.get('dia').setValue(this.celulaDetalhe.celula.dia);
      this.formCelula.get('cep').setValue(this.celulaDetalhe.celula.endereco.cep);
      this.formCelula.get('logradouro').setValue(this.celulaDetalhe.celula.endereco.logradouro);
      this.formCelula.get('numero').setValue(this.celulaDetalhe.celula.endereco.numero);
      this.formCelula.get('complemento').setValue(this.celulaDetalhe.celula.endereco.complemento);
      this.formCelula.get('bairro').setValue(this.celulaDetalhe.celula.endereco.bairro);
      this.formCelula.get('estado').setValue(this.celulaDetalhe.celula.endereco.estado);
      this.formCelula.get('uf').setValue(this.celulaDetalhe.celula.endereco.uf);
      
    })
  }

}
