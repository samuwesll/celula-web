import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { fadeInOut } from 'src/app/shared/const/animations';
import { CelulaRequest } from 'src/app/shared/models/Celula';
import { Igreja } from 'src/app/shared/models/Igreja.model';
import { Rede } from 'src/app/shared/models/Rede.model';
import { UsuarioResumido } from 'src/app/shared/models/Usuario.model';
import { CelulaService } from 'src/app/shared/services/celula.service';
import { ChurchService } from 'src/app/shared/services/church.service';
import { RedeService } from 'src/app/shared/services/rede.service';
import { UsuariosService } from 'src/app/shared/services/usuarios.service';

@Component({
  selector: 'app-cadastrar-celula',
  templateUrl: './cadastrar-celula.component.html',
  styleUrls: ['./cadastrar-celula.component.scss'],
  animations: [fadeInOut],
})
export class CadastrarCelulaComponent implements OnInit {

  formCelula: FormGroup;
  diasSemana: String[] = ['DOMINGO', 'SEGUNDA', 'TERCA', 'QUARTA', 'QUINTA', 'SEXTA', 'SABADO'];
  listaRede: Array<Rede>;
  listaRedeFiltrada: Array<Rede>;
  listaIgrejas: Array<Igreja>;
  listaLideres: Array<UsuarioResumido>;
  listaSupervisores: Array<UsuarioResumido>;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private celulaService: CelulaService,
    private _snackBar: MatSnackBar,
    private redeService: RedeService,
    private igrejaService: ChurchService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.criarFormulario();
    this.carregarRedes();
    this.carregarIgrejas();
  }

  criarFormulario(): void {
    this.formCelula = this.formBuilder.group({
      dia: ['', Validators.required],
      horario: ['', Validators.required],
      nome: ['', Validators.required],
      cep: ['', Validators.required],
      logradouro: ['', Validators.required],
      numero: ['', Validators.required],
      complemento: [''],
      bairro: ['', Validators.required],
      estado: ['', Validators.required],
      uf: ['', Validators.required],
      idIgreja: ['', Validators.required],
      idRede: [{ value: '', disabled: true }, Validators.required],
      idLideres: [{ value: '', disabled: true }, Validators.required],
      idSupers: [{ value: '', disabled: true }, Validators.required],
    })
  }

  btnVoltar(): void {
    this.router.navigateByUrl('celulas')
  }

  cadastrarCelula(): void {
    const {
      dia,
      horario,
      nome,
      cep,
      logradouro,
      numero,
      complemento,
      bairro,
      estado,
      uf,
      idLideres,
      idSupers,
    } = this.formCelula.value;
    let celulaRequest: CelulaRequest = {
      nome,
      dia,
      horario,
      endereco: {
        cep, logradouro, numero, complemento, bairro, estado, uf
      },
      idLideres,
      idSupers,
    }
    this.celulaService.cadastrar(celulaRequest).subscribe(response => {
      this.openSnackBar(response.sucesso.mensage + '', "Cadastro ok")
      this.btnVoltar();
    }, erro => {
      this.openSnackBar(erro.error.errors[0].mensagem + '', "Erro para cadastrar")
    })
  }

  carregarRedes(): void {
    this.redeService.listarRede().subscribe(response => {
      this.listaRede = response.sucesso.value;
    })
  }

  carregarIgrejas(): void {
    this.igrejaService.listarIgrejas().subscribe(response => {
      this.listaIgrejas = response.sucesso.value;
    })
  }

  openSnackBar(mensagem: string, title: string) {
    this._snackBar.open(mensagem, title, {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    })
  }

  selecionarIgreja() {
    const { idIgreja } = this.formCelula.value
    this.listaRedeFiltrada = this.listaRede.filter(rede => rede.igreja.id == idIgreja)
    if (this.listaRedeFiltrada.length == 0) {
      this.openSnackBar("Não há redes cadastrada para essa igreja", "Erro");
      this.zerarCamposDinamicos();
    } else {
      this.formCelula.get('idRede').setValue('');
      this.formCelula.get('idRede').enable({ emitEvent: true })
    }
  }

  selecionarRede() {
    const { idRede } = this.formCelula.value
    this.usuarioService.listarPorRede(idRede).subscribe(result => {
      this.listaLideres = result.sucesso.value;
      this.listaSupervisores = result.sucesso.value.filter(l => {
        const idsExclução = [2, 3, 4, 7]
        return idsExclução.includes(l.perfil.id)
      })
      this.formCelula.get('idLideres').setValue('');
      this.formCelula.get('idLideres').enable({ emitEvent: true });
      
      this.formCelula.get('idSupers').setValue('');
      this.formCelula.get('idSupers').enable({ emitEvent: true });
    }, error => {
      this.openSnackBar(error.error.errors[0].mensagem, "Erro")
      this.zerarCamposDinamicos();
    })
  }

  zerarCamposDinamicos(): void{
    this.formCelula.get('idRede').setValue('');
    this.formCelula.get('idRede').disable({ emitEvent: false })

    this.formCelula.get('idLideres').setValue('');
    this.formCelula.get('idLideres').disable({ emitEvent: false })

    this.formCelula.get('idSupers').setValue('');
    this.formCelula.get('idSupers').disable({ emitEvent: false })
  }

}
