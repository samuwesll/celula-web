import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CelulaComponent } from './celula.component';
import { ConsultaCelulaComponent } from './consulta-celula/consulta-celula.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { CadastrarCelulaComponent } from './cadastrar-celula/cadastrar-celula.component';
import { DetalheCelulaComponent } from './detalhe-celula/detalhe-celula.component';


@NgModule({
  declarations: [CelulaComponent, ConsultaCelulaComponent, CadastrarCelulaComponent, DetalheCelulaComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class CelulaModule { }
