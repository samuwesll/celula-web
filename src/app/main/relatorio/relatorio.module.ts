import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelatorioComponent } from './relatorio.component';
import { ConsultaRelatorioComponent } from './consulta-relatorio/consulta-relatorio.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [RelatorioComponent, ConsultaRelatorioComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class RelatorioModule { }
