import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Relatorio } from 'src/app/shared/models/Relatorio.model';
import { Usuario } from 'src/app/shared/models/Usuario.model';
import { RelatorioService } from 'src/app/shared/services/relatorio.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-consulta-relatorio',
  templateUrl: './consulta-relatorio.component.html',
  styleUrls: ['./consulta-relatorio.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class ConsultaRelatorioComponent implements OnInit {

  formGroup: FormGroup;
  dataSource: Array<Relatorio>
  columnsToDisplay = ['celula', 'lider', 'membros', 'visitantes', 'ofertas', 'quilos', 'status'];
  columnsDias = ['id', 'data', 'membros', 'qtdVisitantes', 'vlOferta', 'kgAmor', 'observacao', 'status']
  expandedElement: Relatorio | null;
  usuario: Usuario;
  param: String;
  emitirRelatorio = false;
  
  ano: number;
  mes: String;

  constructor(
    private formBuilder: FormBuilder,
    private storage: StorageService,
    private relatorioService: RelatorioService,
  ) { }

  ngOnInit(): void {
    this.criarFormulario();
    this.buscarUserLocalStorage();
    this.realizarBusca();
  }

  criarFormulario() {
    const data = new Date();
    let ano = data.getFullYear();
    let mes = data.getMonth() +1;
    this.formGroup = this.formBuilder.group({
      periodo: [`${ano}-0${mes.toString().padStart(1, "0")}`]
    })
  }

  realizarBusca() {
    this.montarParam();
    
    this.consultaRelatorio();
  }

  buscarUserLocalStorage() {
    this.storage.getItem("USER").subscribe((resp: Usuario) => {
      this.usuario = resp;
    })
  }

  montarParam() : void{
    const dataInicial  = this.formGroup.get('periodo').value;

    this.param = `?anoEMesInicio=${dataInicial}`;

    this.param += `&anoEMesFim=${this.proximoMes(dataInicial)}`;
  }

  proximoMes(mesEAno: String): string {
    const ano = Number.parseInt(mesEAno.split("-")[0]);
    this.ano = ano;
    const mes = Number.parseInt(mesEAno.split("-")[1]) + 1;
    this.mes = this.mesString(Number.parseInt(mesEAno.split("-")[1]))
    const data = new Date(ano,mes);

    let proximo = new Date(data.setMonth(data.getMonth(), 1));
    
    return `${proximo.getFullYear()}-${proximo.getMonth()}`;
  }

  consultaRelatorio(): void{
    this.relatorioService.gerarRelatorio(this.param).subscribe(response => {
      this.dataSource = response.sucesso.value;
      this.somarPeriodo();
    })
  }

  mesString(mes: number) : String {
    mString: String;
    switch (mes) {
      case 1:
        return 'Janeiro';
      case 2:
        return 'Fevereiro' 
      case 3:
        return 'Março'
      case 4:
        return 'Abril';
      case 5:
        return 'Maio';
      case 6:
        return 'Junho';
      case 7:
        return 'Julho';
      case 8:
        return 'Agosto';
      case 9:
        return 'Setembro';
      case 10:
        return 'Outubro';
      case 11:
        return 'Novembro';
      case 12:
        return 'Dezembro';
    }
  }

  convertePeso(peso: number): string{
    return peso.toFixed(3).concat(' kg');
  }

  mascaraValor(valor: number): string{
    return Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valor)
  }

  apresentarLideres(lideres: Array<Usuario>): String {
    var nomes: String;
    if (lideres.length == 0) {
      return ''
    }
    lideres.forEach((lider, id) => {
      let primeiroNome: String = lider.nomeCompleto.split(" ")[0];
      id == 0 
        ? nomes = primeiroNome 
        : (id +1) == lideres.length ? nomes = nomes + ` e ${primeiroNome}`
        : nomes = nomes + `, ${primeiroNome}`
    });
    return nomes;
  }

  somarPeriodo(): void {
    let rel: Relatorio = {
      celula: 'TOTAL',
      lider: null,
      membros: 0,
      ofertas: 0,
      quilos: 0,
      visitante: 0,
      status: false,
    }
    let qtdEntregues = 0;
    this.dataSource.forEach(d => {
      rel.membros += d.membros;
      rel.ofertas += d.ofertas;
      rel.quilos += d.quilos;
      rel.visitante += d.visitante;
      if (d.status) {
        qtdEntregues++;
      }
    })
    if (qtdEntregues == this.dataSource.length) {
      rel.status = true;
      this.emitirRelatorio = true
    }
    this.dataSource.push(rel)
  }

  generatePdf(){
    const documentDefinition = { 
      pageOrientation: 'landscape',
      pageSize: 'A4',
      // layout: 'lightHorizontalLines',
      info: {
        title: `relatorio_celula`,
        author: this.usuario.nomeCompleto,
        subject: 'subject of document',
        keywords: 'keywords for document',
      },
      content: [
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAYAAAA8AXHiAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAGPxJREFUeJztXQlQVleW/hOC7CCriMDPDwFkRzZFQBDZdxBQBKEQxYVFXEBREidRWkR6xAXSKGI6LsFO01MVzcTEoB2TVDsdJ+nRaEKjwYnGNpiZkFQ5dhlnmHPs99K0hfH3f+/c94D3VZ0KFeS985177rnn3HcXlUqBAgUKFChQoECBAgUKFChQoECBAgXMcO3aNb0dO3ZoSktL01NTU8s1Gk359OnTywsKCkoXLVoUtnfvXmupdWxvb7fJzc0NycvLK/H09HyoY3JycnlZWVlmU1OT5vLly3pS66iAw40bN/ShoYJnz57d6Ofn129hYTH8zDPPDMOvHv5XT09v2NnZ+UcvL6+zcXFxpWvXrrVlrWN9fb0dvtvHx+d91AV1Qt14QZ1R9/Dw8EZwvOCBgQF91joq4NDd3W2cnZ1d7uHhcc7U1PQu70w/J9ig9vb2V9PT04tY6ZmSklLg4OBwDd/9JP2Qg4mJyV3kBFG26tixY8as9FQAgKHuF46Ojne0cSbVIw2H8txzzw3DEPSv1HpGR0f/Bt+lrZ68fvzPyBE6wQ5qPSc0Nm3aZDN37txyFxeXKyMbQBfBv3322WeHYehp//Wvf20gtq4QaQwiIyPb8R1C9URRq9VXwEnLYUi1EVvXCQtIaI3j4+NLnZycrmHvV+nYSKOJkZHRg6SkpBfE1jktLe0FfLaYuiJ3yM8GMFe7dOmSMkQKQUlJiTooKOhNyDvuCen5jxN85pQpU4ZWrVrlL5bOlZWV/pDHDYmtLx+90BZok5UrV7qKpfOEATS0DQxTXVAt3VWJ7EyPCjZWcHDwYbF0DwkJOUytMwraZubMmV0rVqxQhscn4eLFi3oLFy709/b2PsWX5CoGjgUR5k5vb6/gEv/cuXP6U6dOvUOtM6835nBeXl6n8vLy/GF4VObAHoc5c+a8ZGlpOYQGUzFoHNWIBqqqqooVqv+aNWtiWerO6z958uQhKBa2CNV/3GHLli02kDf8h4ozlIphw/AiRhKPSbsU+vPvRBt2dXWphfIY8+js7NTLz8+PcnBw6JfKoXhJSUk5IZRPRkbGCSl5cHNfXy5evDjmwIEDE3doTEhI2A5J6JBKQofiBYYSwY4VFRV1QmoeKGhTtK1QPmMOt2/f1oNGqDQzM7svdaTiBYYxwY6VlZUlacTiBXUwNTW9HxMTU33z5s2J8d0RElyb6OjoHiMjI8kbYKRkZ2fXCuWGs+NycCxeDA0Nh9HWq1evHt9TElu3btX39fXtmDRpkmRJ+miCumzcuFFwVVhTUxPBuip8kujr6w/7+Ph01NXVjc/I9eKLLxq7u7sfFvuzjFDhP/ZevHhRsOG/+OILfWdnZybzWE/DD23u6up6GNtAKEdZob293RCIvSWnKDVS4uPj94nFNTY2dpfcePKfgzQazVs7d+40FIurpNixY4dNWFhYB4ZkuRkcxcbG5i+Q93mJxbe2ttYbnnlTblxRH2wDbIumpqaxn3P5+fkdMzAwkNywowkUEEN5eXnpYnPOzMxMMTExGRK6vIdCsC1mzJjxG7E5M0VOTk45JOqymVIYKZhkJyUl7S4rKxN9IrG8vFwvNTV1N6vvnU8jXOS6j6tvxeZNjv7+fr2IiIiVGBFUMjAmL/x3QRiqvktISKggNoMqMTGxwtbWVpaRCyLq99HR0ZUDAwNjZ4a+uLjY387O7qmXDlMLDgNQtV0pKiqaT2yCnwDvyoGq8zJOsbDiqY1wa9G+XbJkSQCxCcTB+fPnjT08PB5+UJZa+Ahlbm5+PzAwsBeG5gXNzc1W5EZ4BK2trdb47pCQkPctLS3/Ty4dDvXAtjp79qz8pyFSUlK6pf4Qy78ft1WFh4f/U09Pj5oBda3w2muvOcbFxW3nt6nJwckgH3yDnrkAzJs3rwhLWpWERsKEGaLCD76+vrv37NnjzoC2Tmhra3P38fHZbWVl9YPUs/XYZgUFBSsZ0H561NTUeFlbW9+Sqgdy68ExQnVUVFQEf/nll7JPSjFxXr58eWBoaGibqampZBEM3wkOfmv16tWizeeJgvb2duOgoKB92mzKpBIHB4chqEQrjh49Kv984RHg5tuoqKiKadOmib4JQxvhd4xjG3Z0dMjHfrGxsRukWK2ABsF5MkiKO+vr6y3YsKVDQ0ODBTRuB1Svksz9YRtCOlPPhu0TsGPHDjMop5l/wuDWHA1BItx8/fp1+fQygejr68N9lM04c6+SoKM6OzvfhvxP+k4aExOznbVTcZOc17OzswUvdZErcnNzY21tba9L0WFhBGpmRHN0VFZWJkIDM+tZfHILw8WFDRs2qFnxlAowvKuDg4MvsHYubNOqqqpERjT/ETAE6kO53MuyVMZ3QYL7QWdnpx0rnlJj9+7dNhC5elk6F7dfsbexsZH94sDk5OQ4SPbui0FEG8GqRa1Wvzl//vwprDjKBZBQ27i4uLzJsurGts3MzExixfEngEcfEYOAtgKG/TQnJ8eBET3ZYeXKlc6enp5XVIzsjRHS29v7GCt+D7Fq1SovnNCjIPSocB9L+3bt2qVmRlCmaG1tVU+dOvXPrI4ewAnnFStWsJs0hRL/MKsxHxNJIBfFjJzMUV5ePptVwYRtjG3NhFhTU1Pw5MmTmZAyNja+n5qayuxYx7GCtLS0IrQNi86Nbb19+/ZgclJ+fn67qMmg4Pql2bNnHyQnNEYRGRnZxWIfAT4fqv9dpGSg/LSGMX6ARU9xd3fvxeOrSQmNYaBtPDw8yKch8PkODg7XcVMMGRkoP3MMDAxIF6rhs83MzO7W19eLdsreeMXmzZv9zc3NyQ+nw5W38+fPzychgQeTBQYG7mfRQ4KCgl4lITEOAW3SpSJ2LJSAgIADp0+fFn/CNCsrK9ja2pp0ly+3K/nG+vXr1aITGKeoq6tTo82oOzy2fUZGhvhJPFRnm6hnfiFhf5CQkFAtuvLjHGgztJ2KsG2w7VNSUjaJrry3t/c56tzK1dX101deecVMdOXHOX71q1+Zubm5faoiHk3QB0RV/KuvvrKwtbUlK235VQsLFy4sFVVxHXHmzBmLkpKSzaGhoSc8PT0/9/X1RacfhsYbxp+hGrsSHh7+9uLFi+s//vhjWVSuixYtKqUeDm1sbIbRF0RTurCwMJFyPTb32eYvv//97yVbsLd161ZHGO5XQZJ6wcrK6h8uUXqcztxacbxg6Y9JSUkrt23b5iiV/mg7sCHpngN8dkFBgXjLaWbNmkU6KYrLNIqKil4WTeGnQGdnp3laWloTNMptIUcsYQ5iZ2d3Ozk5efu+fftMpeACNnyJOmrNnDlTvMlSbmwlU9be3v5uT08P8+UwpaWlAc7OzieNjY0fiBGR8e+NjIx+hOHyg6VLl85gzefo0aM2aEshHJ4kXl5e4uRZBw8ejABlBymVhaGkQxRltcT+/fv1IUpV4UQsBR9+kjcrK2tNW1sb08VyaEvKtAUi+zcHDhyIEKxofn7+EktLy/+lUBIFvwnC8MHsLIWWlhY9SMpXmpubkxcj4Fx4ncrKhoYGZnscIdfDryNk3MAXHuTm5govsgIDA/+ZavkxkodK4zbLpDcmJmaloaHhPQo+o/GDofHenDlzmO0yxiIEbUrFCX0BfEJ4nuXj40M2f4XPhdDdK1hJLbFmzZpcjFT8uyk4jcYRI1dNTU0uK56Q452m5CN4Puudd94xgUrnc6pGwOOiIyIixJ/NHQUbN25UQ674gxQbQbGXW1tb/xd+fmHBFW1KeYoirug9e/as7hPZ9fX1LjCmfkulIDz7DryDfHUoXt4dEBDQTcVDG+Gic3dfXx95Mo82nTx5Mtl3XQsLiztr167VvZMUFhbOsLKyIkvcNRrN55999hn5NMPq1avD8AQaKh7aCHdL1w+VlZVh1Hw//fRTK2dn58tUXDCBX7hwYaDOCpaVlc2nPIkuLCzsdzorpyUOHTpkDJHiD3I4hwoF8p/zR44cIZ9ARdtScUafWLZsme45Y15eXjmFYrzExsaS39AOPSvE1NT0rlwcC4qHv8JIQB610LaURRfnG7ohPT19K4VivCxevJj85N6ZM2c2UnJ4Wnnmb1cFNxLTVqFtKTtTZmbmNp2Vi4uL66BSDKW4uJh89wcMPWS5hi6Cje3v7/9nYtp4UXswpWNxvqEbwABkjoUffE+ePOmrs3Ja4NKlS8YODg6SO9NIwcaeOnXqMCTYpCs5wLbelPcWcb6hG6BRSByLnzTEewt1Vk4LtLW1BUt9vufj+INupHnW4ODgs2hjKg6cb+gGUIzMsezt7Yd1VkxLrFu3bqFckvZHpba2tpCUPICzMYlAEaK7Y0EoJXMsjUZD7li5ubmyuphypLC4coSzMYlwvqEbYBghy7Gef/55csdKSEggnS4RIrGxseSOxdmYRPT09HR3LO6PSRRjEbG4qCC5E40mGRkZEzdimZqakjkWV62Roq6urkyOQyHqBLqROxZlRWxiYqK7Y0FZTOZYXMVCitbW1lC5VYX8/T4tLS3ks+/8EiEKgcJAd8fCi8GpFMM5lnfeeYf0UK/Lly+bT5kyRXJnGinoWHZ2dsMXL140p+R++vRpL8rrZzjf0A0xMTFkjoUGrq6uJp959/b2vk7FQRdB3tOnT79OShpQVVVFOvPO+YZuSE9P/wWVYijFxcVLdFZOS0DPkt23QnB28m+FBQUFxZSOBb6hO4fk5GTq1Q3kq0fBAPGGhoak5xo8jRgZGT1ISUmJp+aNtqVc3cD5hm5YsmRJHuU4HRYW1q2zclqiq6vL3N3d/WM5VIeoA0TQz1599VXyK0VCQ0Nfp1yPtXz58oU6K5ednU163qharf73b7/99hmdFdQSaWlpEbhbhoqHtoI6QLQSvifvCRgcHDR0dXX9ExUP9Ak81kpnBWtra10tLS3/m0pBa2vrwe3bt0fqrKCWOHPmDB4ad4qKh7bi5+d36t133yVf875t27bZ0G7fUPGwsrL6bsOGDW46K/jee+9Z2Nvb91MpiLt05s2bx2SXDhjbHwwiyUpSfCc09N1Nmzb5sOA6d+7cDWBbsr0K6BPQQYQN5xqNhuzcBpwo9PHxIV/3ziM/P3+TFPcq4jtzc3OZdCAE5HFk691ROJ8QBjyKmXIntJ2d3a2WlhZmV5nMnj0bL+18wCJyod3wXfhOVvx27tzpgDal4oecvL29he+EnjNnTjnVgjHuMktc+87sCrM33njDIjw8/F8oV1fygu+IjIw8cfz4cWYXS0JUnkd5HQ36AvqEYEX37t0bZWtrS3qobXBwcJtgRZ8CkHgahIWF1VJ9S8MOg88OCQmp7ejoMGHJDXi9QsGJF/SF1tZW4ZuMv/nmm+dgOPyEUtlp06Z9K1hRHQCRMg7K8u/FPvbHxcVlCCKHJLe/Ojg4kAUB7qvBJ4ODg+IsKedOFyFzLBy3q6urt4ii7FPitddec5o1a9a+KVOm3BWSS+Lf4jNmzpzZdvToUbUUXNCGlKs50LFmzJjRKprC0LNzqJNdtVp968aNG5KdQVpXV+cVERGx0d3dvR+nQVSqx59IM/J8Uvy3zz//fH9UVNQmGGK97ty5Qz7hOxquXr1qjDakbqfCwsIc0ZSGXm1jYWFBevQPJrqVlZXMjvoZDdeuXdOrqqrSZGVlLQBHedXLy+vfHB0dv8btWpgzoeDP+P/gd+fh3xzKzMxcUFFRocG/lVL35cuXZ6MNKdsI+R86dEjcU6L9/PzIL72GHncOkl0jURWfAIBoO0mj0XygImwb7jvnBdGVx63q1I5lbGx8b9myZeKF2gmCsrKyLCMjo/9RETsWVJziL/fJyMiYZWdnR37TFAwzfUuXLpXFofxjAWgrJyenPupOj22fnp4+S3QCvb29xgEBAb+lVF7F9Yzo6OidohMYp4iJiWlh8RUhMDCw5/Tp0zTFFXhsAeURhCrVTwfe3l2/fr2GhMQ4AhQ7arSVitipcP0VVINlZEQaGhogItp9RU0EBaquN1966SXJph/kjsbGRmO0kYrBCGJra/ufGzZssCUjMzg4+AxUh0dYhF5cFBcREVFPRmaMg/uYTr54kasGj5DPz23bti2K8iPnSEImJib3srKyMkkJjUFAIZUOFTSTtWXY1i+//DL5AcQPERISQnp3oUr199ltnNWuqqpifs+OXLF27dopnp6e11gtWIS2/oAZuSVLlgSwWjCHBgRD9nV2dk74KYhjx47ZoS1YOBV3q8bwihUrgtgxVD3cBfIHCkKPCh+53N3dT3V1dRkyIygzHDx40FCj0bylYmBzXoKCgv7AiN7fAeXnQgMDgx/FIPAkQcfCbWj+/v4969atm3CRa/369TY4j0S5Fe9RwbZdtGiR7lu8dAUkdAZubm7vs9ycgJdNQuTqbWlpmTDTEPv37zdGztQXvY8UbFO1Wn1my5YtBqx4/gMKCgoWT548mXyCTjWCMPba6dOnt4PB7VnxlAow/Nl7eXm1s1hGPVKwTRcsWLCYEc3RASG6mfLixdEEe6+Tk1N/Tk4O+aEiUiEzM3OGs7NzvxTHMM2YMaOFDcufwZ49e7xwzxxLx+Id2dXV9fqyZctSWXFlhaVLlya7uLhck8KpMFrt2rWL9HgprREcHLyJ8s6dxwm3y+c+VKiN7e3tYz7v6unpMYuMjNxhZmZ2X4qNtdiGUAky2//4RLS1tVnMmjXrlFS7jFFg2LheWVkpyQYGMVBdXR0LCfN1KW2Ibbh3715mW9W0QklJib+tre2QVKe6cNvYH0CPbywsLFQzIS0CioqK1FFRUY0QpSQ7tIT70DyEbciC81MjJSVlDeYFUh4ZhIk97n2LiYlpam5ullfvG4Hjx4/bJCcn70JdWU4ljCbYZomJiTUMaOsGSPpMoKJ4Tw5nURkaGv4IjXYhPT19Fc4F0bPXDnh/Ym5ubrW9vf0nJiYmw6wr6tEEctRzTU1NsrHRqIAx2lij0fSpJHYsXrjzIe76+fkdhgaNlcLJOjs7jYuLixP9/f0Poy5SOxIvqIebm1s/2ITpbm2dERsbmwLVmuQHnfHCRwU8QRmqni5wMDW1DXjk5eWpQ0JCuvDdI/cjsuL+c4J53dy5c1PIjSAWTp48qQc5zgtYNqtkYMCRwl0QdQvymwW0Vnh4kuACeNfXcjtrXvU3p7ofHx//8ltvvSXpfkidgAdvyKmHjhT8NAT6FVBxh6hdzPKj8dMKcGd2tJLoGBgYwJ09vay/dWkrlpaWQ1VVVaLP3FdXV6fgs6XmN5pgW2Cb4JZ8sXkzBSStNi4uLh1Sl9SPE0dHx5vl5eVqsfjCs5zhmV/JLUqjPtgG2BbYJmLxlRSrVq1y8vHxkU2lOFKwB0dHR9eJxTU8PLyG+gwFXYQ7gqhvxYoVTmJxlQW6u7vVERERF+QYuZycnL6+ceOG4NOMb968qe/s7HxTaj4jhY9U4PAXjh07phbKUZZAYh4eHn+SW5WEEaa5uVnw54xf/vKXfnJL2LkzQ6+8/vrr49OpeJSUlEwF52K6zFYbiYuLWy6U25w5c0ql5sELvyjS3d29p6KiYppQbmMCp06d0k9KStovpzwkJyfnhFBe+fn5J+TAiZ/iSU1NPfT222+TX1ggO+ARjWZmZqTH72greLqxUD4QsU5IzQMFbIpHVu4TymfM4siRI5Ogl2fY2tp+LfVEamJiomDHSklJkTRicctfvs7Ozk7v6OiYeJFqJAYGBvQWLFgQD5XZFSmT+vnz5+8WyiU5OZn8cLqfE6hIP8/Ly4v/4osvxt5nGip88sknxhC+95ibm9+TYv38smXLEoRyqKqqSpJCd7RZWFjY3kuXLo3t2XQqHDx4UB8vWLS3t2dyVJJK9dPK07++//77VkL1P3/+vI21tfVfWeqOtkpISCjfv3//xB76tEFpaaljQEBAD96LR5174bMDAwN/K5buISEhPVS6jhQ8xdrPz6+nvr7eVSzdJwwaGhoi3Nzc+qici9vlcy8nJydMLJ2hU4TjOiex9R25lsvV1bVv8+bN5Bdqjmvgkllo+EYbG5sbYif3kyZNepCUlLS7v79ftGHk6tWr+lAd7sZni6krfpZBG0Cq8GJnZ6eZWPpOeFRXVzvOmzdvCx7eLzSC8Q7q6+vbBUOJ6CfYvPDCC4be3t4HhOrJ//20adO+jouLewmKg/H1AVku+PDDD/WWLl0aGBwc3I331qgE9H4PD4+za9assaPSFZ/t6el5RkiURY5BQUHdMLwGnjt3TplCYIGenh6H2NjYWo1Gc8nIyOj+z0UH/nfYyHZ2drfT0tLWs9IzMzOzDoawvzxpWxyvI3KBvPJyTExM7fHjx5ldEKrgEXR3d5tB3rEeosMfzc3Nv3t0WQ5/kZKlpeUtiHS/KysrY3Oe5gjgO0NDQ38HVe4t/gIo1SMRFKq8793d3c8nJCSsO3XqlOCpDwUi4aOPPnouLy/PLSMjowgiWQM0UAdEpo7IyMhV0dHRUevWrZO899fU1DhArhQ9d+7cStQNdYTItCk9Pb2wqKjI/d1331WGOwUKFChQoECBAgUKFChQoECBAgUKFChQoECBAgUKFChQoOBR/D/+EOPsKi0OLAAAAABJRU5ErkJggg==',
          width: 60,
			    height: 60,
          alignment: 'right',
        },
        {
          text: this.usuario.igreja.nome,
          bold: true,
          fontSize: 24,
          alignment: 'center',
          margin: [0, 0, 0, 5],
        },
        {
          text: `Rede ${this.usuario.rede.cor}, aos cuidados de ${this.usuario.rede.pastores}`,
          fontSize: 14,
          italics: true,
          alignment: 'center',
          margin: [0, 0, 0, 5]
        },
        {
          text: `Relatório células - ${this.mes} ${this.ano}`,
          bold: true,
          italics: true,
          fontSize: 22,
          alignment: 'center',
          margin: [0, 0, 0, 25]
        },
        {
          table: {
            widths: ['*', '*', '*', '*', '*', '*'],
            body: [
              [{
                text: 'CÉLULA',
                style: 'tableHeader'
              },
              {
                text: 'LIDERES',
                style: 'tableHeader'
              },
              {
                text: 'QTD. MEMBROS',
                style: 'tableHeader'
              },
              {
                text: 'QTD. VISITANTES',
                style: 'tableHeader'
              },
              {
                text: 'VL. OFERTAS',
                style: 'tableHeader'
              },
              {
                text: 'KG. AMOR',
                style: 'tableHeader'
              }],
              ...this.dataSource.map(rel => {
                return [rel.celula, rel.lider, rel.membros, rel.visitante, this.mascaraValor(rel.ofertas), this.convertePeso(rel.quilos)]
              }),
            ]
          }
        }
      ],
      
      styles: {
        tableHeader: {
          bold: true,
          fontSize: 8,
          alignment: 'center',
          margin: [3, 3, 3, 3]
        }
      }
    };
    
    pdfMake.createPdf(documentDefinition).open();
  }

}
